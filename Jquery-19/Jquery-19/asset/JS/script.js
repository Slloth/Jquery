﻿$(document).ready(function () {
    var essai = 0, index = 0;
    var resultat = Math.floor((Math.random() * 100) + 1);
    var regexNumber = /^[0-9]+/;
        $("input").click(function () {
            essai = $("textarea").val();
            index++;
            if (regexNumber.test(essai) == false) {
                alert("ce n'est pas un nombre.");
            }
            else if (essai == resultat) {
                alert("Bravo, vous avez trouvez " + resultat + " après " + index + "!!!");
            }
            else if ((essai > 0) && (essai <= resultat)) {
                alert("Non c'est plus haut!");
            }
            else if ((essai >= resultat) && (essai < 101)) {
                alert("Non c'est plus bas!");
            }
            else {
                alert("Votre nombre n'est pas comprit entre 1 et 100.");
            }
        })
});